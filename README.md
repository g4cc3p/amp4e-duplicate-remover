NOTES: 
-	api.cfg and find_duplicate_guids.py must be saved on the same directory.
-	On the api.cfg file enter generated API credentials.

Requirements to run the script:
Python 3.x
requests python library.

To install requests library please run the following command:
> pip install requests
OR
> pip3 install requests

USAGE:
> python ampdupremover.py
