'''
Script: AMP4E Duplicate Remover v1.0
Modified version of  https://github.com/CiscoSecurity/amp-04-find-duplicate-guids
Author(s): German A. Castellar Cepeda

'''

from collections import namedtuple
import configparser
import json
import requests
import datetime

def process_nested_json(getresponse_json, parsing_container):

    def process_getresponse(guid_json, parsing_container=parsing_container):
            '''Process the individual GUID entry
            '''
            connector_guid = guid_json.get('connector_guid')
            hostname = guid_json.get('hostname')
            last_seen = guid_json.get('last_seen')
            
            #Time format modified from ISO8601 to Python datetime 
            formatted_lseen =  datetime.datetime.strptime(last_seen, "%Y-%m-%dT%H:%M:%SZ")
            
            network_addresses = guid_json.get('network_addresses')
            parsing_container.setdefault(hostname, {'macs':[], 'mac_guids':{}, 'guid_last_seen':{}})
            parsing_container[hostname]['guid_last_seen'][connector_guid] = formatted_lseen

            for network_interface in network_addresses:
                mac = network_interface.get('mac')

                parsing_container[hostname]['macs'].append(mac)
                parsing_container[hostname]['mac_guids'].setdefault(mac, set())
                parsing_container[hostname]['mac_guids'][mac].add(connector_guid)

    for guid_entry in getresponse_json['data']:
        if 'network_addresses' in guid_entry:
            process_getresponse(guid_entry)

def duplicates_finder(parsed_data, duplicate_container):
    ''' Analyzes the parsed_computers container and looks at how many times each MAC Address
        appears for a given hostname. If the same MAC appears more than once the host is
        added to  the duplicate_computers container.
    '''
    for hostname, data in parsed_data.items():
        macs = data['macs']
        for mac in macs:
            if macs.count(mac) > 1:
                for guid in data['mac_guids'][mac]:
                    host_tuple = namedtuple('host_tuple', ['hostname', 'guid', 'last_seen'])
                    last_seen = parsed_data[hostname]['guid_last_seen'][guid]
                    duplicate_container.add(host_tuple(hostname, guid, last_seen))
                 

def format_duplicates(raw_duplicate_container):
    ''' Processes the duplicate_computers container based on hostname
        filters olders for removal
    '''
    hosts = {}

    for host_tuple in sorted(raw_duplicate_container, reverse=True):
        hostname = host_tuple.hostname
        guid = host_tuple.guid
        last_seen = host_tuple.last_seen
        hosts.setdefault(hostname, {})
        hosts[hostname][guid] = last_seen
    
    return hosts


def filter_old_duplcates(hosts):

    duplicates_per_host = {}

    # Creates dictonary of older guid entries
    for hostname, dupes in hosts.items():
        for idx, (guid, last_seen) in enumerate(dupes.items()):
            if(idx < len(dupes) - 1):
                duplicates_per_host.setdefault(hostname, {})
                duplicates_per_host[hostname][guid] = last_seen
    return duplicates_per_host


def print_duplicate(formatted_duplicate_container):
    '''Process formatted_duplicate_computers and print duplicate stats by host name
    '''
    print("\n\nNOTE: GUIDs with oldest LAST_SEEN will be removed. ")
    print('Hosts with duplicate GUIDs found: {}'.format(len(formatted_duplicate_container)))
    for host, dupes in formatted_duplicate_container.items():
        print('\n{} has {} duplicates'.format(host, len(dupes)))
        print('{:>20}{:>36}'.format('GUID', 'LAST SEEN'))
        for guid, last_seen in dupes.items():
            print('  {} - {}'.format(guid, last_seen))            
         

def delete_duplicates(session, delete_guid_url, duplicates):
    
    print('\n')
    option = input('Proceed removing duplicates(Y/N):')
    if(option == 'Y'):  
        for host, dups in duplicates.items():
            for  guid in dups.keys():
                response = session.delete(delete_guid_url + '{}'.format(guid))
                print(response)
    else:
        print('None duplicates removed.')

def get(session, url):
    '''HTTP GET Request to gather computers 
    '''
    response = session.get(url)
    response_json = response.json()
    return response_json

def main():
    '''
    Script Orchestrator
    Workflow:
        1. Gathers computers data(guid, network details, last seen, hostname)
        2. Find MAC duplicates in GET response
        3. Remove old duplicate entries
    '''

    # Specifies the config file
    # Keep file under the same python script directory
    AuthAPI_file = 'api_authentication.cfg'

    # Reads API KEY and CLIENT ID 
    config = configparser.RawConfigParser()
    config.read(AuthAPI_file)
    client_id = config.get('AMP4E', 'client_id')
    api_key = config.get('AMP4E', 'api_key')

    # Create session object
    amp_session = requests.session()
    amp_session.auth = (client_id, api_key)

    # Containers for data
    parsed_computers = {}
    duplicate_computers = set()

    # API URL to GET GUIDs
    computers_url = 'https://api.amp.cisco.com/v1/computers'

    # API URL to DELETE GUIDs
    delete_guid_url = 'https://api.amp.cisco.com/v1/computers/'

    # GET API Request to fetch Computers data
    get_response = get(amp_session, computers_url)
    
    # Displays the TOTAL of GUIDs found
    total_guids = get_response['metadata']['results']['total']
    print('GUIDs found: {}'.format(total_guids))

    process_nested_json(get_response, parsed_computers)
    duplicates_finder(parsed_computers, duplicate_computers)
    formatted_duplicates = format_duplicates(duplicate_computers)
    print_duplicate(formatted_duplicates)
    filtered_duplicates = filter_old_duplcates(formatted_duplicates)
    delete_duplicates(amp_session, delete_guid_url , filtered_duplicates)

   
    # Check if there are more pages and repeat
    while 'next' in get_response['metadata']['links']:
        next_url = get_response['metadata']['links']['next']
        get_response = get(amp_session, next_url)
        index = get_response['metadata']['results']['index']
        print('Processing index: {}'.format(index))
        process_nested_json(get_response, parsed_computers)

if __name__ == "__main__":
    main()







